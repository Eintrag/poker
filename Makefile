DIROBJ := obj/
DIREXE := exec/
DIRHEA := include/
DIRSRC := src/
DIRDEBUG := obj/Debug/src/
PROGRAM_NAME := Poker
MKDIR_P := mkdir -p

CC := g++
CFLAGS := -I$(DIRHEA) -c -Wall

all: dirs UserInterface.o Player.o Card.o Hand.o CardDealer.o PokerGame.o StraightPoker.o main.o 
	g++  -o $(DIREXE)$(PROGRAM_NAME) $(DIROBJ)*.o 

dirs:
	$(MKDIR_P) $(DIROBJ) $(DIREXE)
	
run: 
	./$(DIREXE)$(PROGRAM_NAME)

%.o: $(DIRSRC)%.cpp
	$(CC) $(CFLAGS) $^ -o $(DIROBJ)$@

clean:
	$(RM) -rf *~ core $(DIROBJ) $(DIREXE) $(DIRBIN) $(DIRHEA)*~ $(DIRSRC)*~