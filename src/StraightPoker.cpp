/*
 * StraightPoker.cpp
 *
 *  Created on: Nov 5, 2016
 *      Author: sergio
 */

#include "StraightPoker.h"
#include "Player.h"
/*
 * Rules:
 * A complete hand is dealt to each player at the beginning.
 * Players bet in only one round.
 */

StraightPoker::StraightPoker(unsigned int numberOfPlayers) :
		PokerGame(numberOfPlayers) {
	// TODO Auto-generated constructor stub

}

void StraightPoker::startGame() {
	firstDeal();
}

void StraightPoker::firstDeal() {
	CardDealer* cardDealer = new CardDealer();

	for (unsigned int i = 0; i < players.size(); i++) {
		players[i]->giveCards(
				cardDealer->dealCards(NUMBER_OF_CARDS_IN_FIRST_DRAW));
	}
	for (unsigned int i = 0; i < players.size(); i++) {
		userInterface->printCards(players[i]->getCards());
	}
	comparePlayerHands();
}

StraightPoker::~StraightPoker() {
	// TODO Auto-generated destructor stub
}

