/*
 * UserInterface.cpp
 *
 *  Created on: Nov 5, 2016
 *      Author: sergio
 */

#include "UserInterface.h"
#include <clocale>
#include <iostream>
#include <sstream>

namespace std {

UserInterface::UserInterface() {
	setlocale(LC_ALL, "");

}

UserInterface::~UserInterface() {
	// TODO Auto-generated destructor stub
}

void UserInterface::printCards(vector<Card*> cards) {
	std::wostringstream oss;
	for (unsigned short i = 0; i < cards.size(); i++) {
		oss << cards[i]->getValue() << cards[i]->getSuit() << L" ";
	}
	wcout << oss.str() << endl;
}
void UserInterface::printMessage(wstring message) {
	wcout << message << endl;
}

} /* namespace std */
