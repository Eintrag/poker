/*
 * Hand.cpp
 *
 *  Created on: Nov 5, 2016
 *      Author: sergio
 */

#include "Hand.h"

using namespace std;

Hand::Hand() {
	// TODO Auto-generated constructor stub

}
void Hand::addCard(Card* card) {
	cards.push_back(card);
	sortHand();
}
const char* Hand::evaluate() {
	// Consider returning a struct that also contains the highest card that would break a draw
	if (countCardsWithSameValue() == 5) {
		return getTextForEnum(FIVE_OF_A_KIND);
	} else if (cardsFormStraightFlush()) {
		return getTextForEnum(STRAIGHT_FLUSH);
	} else if (countCardsWithSameValue() == 4) {
		return getTextForEnum(FOUR_OF_A_KIND);
	} else if (cardsFormFullHouse()) {
		return getTextForEnum(FULL_HOUSE);
	} else if (cardsFormFlush()) {
		return getTextForEnum(FLUSH);
	} else if (cardsFormStraight()) {
		return getTextForEnum(STRAIGHT);
	} else if (countCardsWithSameValue() == 3) {
		return getTextForEnum(THREE_OF_A_KIND);
	} else if (cardsFormTwoPair()) {
		return getTextForEnum(TWO_PAIR);
	} else if (countCardsWithSameValue() == 2) {
		return getTextForEnum(ONE_PAIR);
	} else {
		return getTextForEnum(NOTHING);
	}
}
const int Hand::countCardsWithSameValue() {
	unsigned int maxNumberOfCardsWithSameValue = 0;
	for (unsigned int i = 0; i < cards.size(); i++) {
		unsigned int numberOfCardsWithSameValue = 0;
		for (unsigned int j = 0; j < cards.size(); j++) {
			if (cards[i]->getValue() == cards[j]->getValue()) {
				numberOfCardsWithSameValue++;
			}
		}
		if (numberOfCardsWithSameValue > maxNumberOfCardsWithSameValue) {
			maxNumberOfCardsWithSameValue = numberOfCardsWithSameValue;
		}
	}
	return maxNumberOfCardsWithSameValue;
}
const bool Hand::cardsFormFullHouse() {
	// TODO implement
	return false;
}
const bool Hand::cardsFormFlush() {
	for (unsigned int i = 0; i < cards.size() - 1; i++) {
		if (cards[i]->getSuit() != cards[i + 1]->getSuit()) {
			return false;
		}
	}
	return true;
}
const bool Hand::cardsFormStraightFlush() {
	return cardsFormStraight() && cardsFormFlush();
	//TODO calculate considering the cards in the hand. Give the highest possible value
	for (unsigned int i = 0; i < cards.size(); i++) {
		for (unsigned int j = 0; j < cards.size(); j++) {
			if (i != j && cards[i]->getValue() == cards[j]->getValue()) {
				return getTextForEnum(ONE_PAIR);
			}
		}
	}
	return getTextForEnum(NOTHING);
}

void Hand::sortHand() {
// Bubble sort
	Card* temp;
	for (unsigned int i = 0; i < cards.size(); i++) {
		for (unsigned int j = cards.size() - 1; j > i; j--) {
			if (cards[j]->getValue() < cards[j - 1]->getValue()) {
				temp = cards[j - 1];
				cards[j - 1] = cards[j];
				cards[j] = temp;
			}
		}
	}
}

const bool Hand::cardsFormStraight() {
	for (unsigned int i = 0; i < cards.size() - 1; i++) {
		if (cards[i] != cards[i + 1] - 1) {
			return false;
		}
	}
	return true;
}

const bool Hand::cardsFormTwoPair() {
	// TODO implement
	return false;
}

const char * Hand::getTextForEnum(int enumVal) {
	return EnumStrings[enumVal];
}
Hand::~Hand() {
// TODO Auto-generated destructor stub
}
