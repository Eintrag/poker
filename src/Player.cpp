/*
 * Player.cpp
 *
 *  Created on: Nov 5, 2016
 *      Author: sergio
 */

#include "Player.h"

using namespace std;

Player::Player() {
	// TODO Auto-generated constructor stub

}

void Player::giveCards(const std::vector<Card*> cards) {
	for (unsigned int i = 0; i < cards.size(); i++) {
		hand->addCard(cards[i]);
	}
}

const std::vector<Card*> Player::getCards() {
	return hand->getCards();
}

const char* Player::getHandValue() {
	return hand->evaluate();
}

Player::~Player() {
	// TODO Auto-generated destructor stub
}

