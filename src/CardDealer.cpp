#include "CardDealer.h"
#include "Card.h"
#include <vector>
#include <random>
#include "cstdlib"

const wchar_t spade[] = L"\u2660";
const wchar_t heart[] = L"\u2661";
const wchar_t diamond[] = L"\u2662";
const wchar_t clubs[] = L"\u2663";

CardDealer::CardDealer() {
	//ctor
}

CardDealer::~CardDealer() {
	//dtor
}

std::vector<Card*> CardDealer::dealCards(unsigned short numberOfCards) {
	std::vector<Card*> cards;
	for (unsigned int i = 0; i < numberOfCards; i++) {
		cards.push_back(this->pickRandomCard());
	}
	return cards;

}

Card* CardDealer::pickRandomCard() {
	std::random_device seed;
	std::mt19937 gen(seed());
	std::uniform_int_distribution<int> distSuit(1, 4);
	std::uniform_int_distribution<int> distValue(1, 12);

	int suitCode = distSuit(gen);
	// TODO improve control flow
	// TODO include jokers
	// TODO consider keeping track of deck (do not repeat cards) instead of generating a random card. Maybe include the possibility of several decks.
	if (suitCode == 1) {
		return new Card(spade, distValue(gen));
	} else if (suitCode == 2) {
		return new Card(heart, distValue(gen));
	} else if (suitCode == 3) {
		return new Card(diamond, distValue(gen));
	} else
	//if (suitCode == 4)
	{
		return new Card(clubs, distValue(gen));
	}

}
