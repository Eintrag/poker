/*
 * PokerGame.cpp
 *
 *  Created on: Nov 5, 2016
 *      Author: sergio
 */

#include "PokerGame.h"
#include <sstream>

using namespace std;

PokerGame::PokerGame(unsigned int numberOfPlayers) {
	for (unsigned int i = 0; i < numberOfPlayers; i++) {
		players.push_back(new Player());
	}

	std::wostringstream oss;
	oss << L"Starting game for " << players.size() << L" players";
	userInterface->printMessage(oss.str());
}

void PokerGame::startGame(void) {
}

void PokerGame::comparePlayerHands(){
	for (unsigned int i = 0; i < players.size(); i++) {
		std::wostringstream oss;
		oss << L"Player " << i + 1 << L" has " << players[i]->getHandValue();
		userInterface->printMessage(oss.str());
	}
}
PokerGame::~PokerGame() {
	// TODO Auto-generated destructor stub
}

