#include <clocale>
#include "StraightPoker.h"
using namespace std;

int main() {
	unsigned int numberOfPlayers = 2;
	StraightPoker* pokerGame = new StraightPoker(numberOfPlayers);
	pokerGame->startGame();
	return 0;
}
