/*
 * Player.h
 *
 *  Created on: Nov 5, 2016
 *      Author: sergio
 */
#include "Card.h"
#include <vector>
#include "Hand.h"

#ifndef SRC_PLAYER_H_
#define SRC_PLAYER_H_
using namespace std;

class Player {
public:
	Player();
	virtual ~Player();
	const std::vector<Card*> getCards();
	void giveCards(const std::vector<Card*> cards);
	const char* getHandValue();
private:
	Hand* hand = new Hand();
};

#endif /* SRC_PLAYER_H_ */
