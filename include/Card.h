#ifndef CARD_H
#define CARD_H
using namespace std;

class Card {
public:
	Card(const wchar_t* suit1, const unsigned short val);
	virtual ~Card();

	const wchar_t getSuit() {
		return *suit;
	}
	const unsigned short getValue() {
		return value;
	}

protected:

private:
	const wchar_t* suit;
	const unsigned short value;
};

#endif // CARD_H
