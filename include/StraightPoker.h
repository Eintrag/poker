/*
 * StraightPoker.h
 *
 *  Created on: Nov 5, 2016
 *      Author: sergio
 */

#ifndef SRC_STRAIGHTPOKER_H_
#define SRC_STRAIGHTPOKER_H_

#include "PokerGame.h"

class StraightPoker: public PokerGame {
public:
	StraightPoker(unsigned int numberOfPlayers);
	virtual ~StraightPoker();
	void startGame();
private:
	const unsigned short NUMBER_OF_CARDS_IN_FIRST_DRAW = 5;
	void firstDeal();
};

#endif /* SRC_STRAIGHTPOKER_H_ */
