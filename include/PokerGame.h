/*
 * PokerGame.h
 *
 *  Created on: Nov 5, 2016
 *      Author: sergio
 */

#include <iostream>
#include <string>
#include "UserInterface.h"
#include "Card.h"
#include "CardDealer.h"
#include <vector>
#include <clocale>
#include "Player.h"

#ifndef SRC_POKERGAME_H_
#define SRC_POKERGAME_H

using namespace std;

class PokerGame {
public:
	PokerGame(unsigned int numberOfPlayers);
	virtual ~PokerGame();
	void startGame(void);
protected:
	UserInterface* userInterface = new UserInterface();
	std::vector<Player*> players;
	void comparePlayerHands();
};

#endif /* SRC_POKERGAME_H_ */
