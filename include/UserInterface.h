/*
 * UserInterface.h
 *
 *  Created on: Nov 5, 2016
 *      Author: sergio
 */
#include <vector>
#include "Card.h"
#include <string>

#ifndef SRC_USERINTERFACE_H_
#define SRC_USERINTERFACE_H_

namespace std {

class UserInterface {
public:
	UserInterface();
	void printCards(vector<Card*>);
	void printMessage(wstring message);
	virtual ~UserInterface();

};

} /* namespace std */

#endif /* SRC_USERINTERFACE_H_ */
