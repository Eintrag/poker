/*
 * Hand.h
 *
 *  Created on: Nov 5, 2016
 *      Author: sergio
 */
#include "Card.h"
#include <vector>

#ifndef SRC_HAND_H_
#define SRC_HAND_H_

using namespace std;

char * getTextForEnum(int enumVal);
class Hand {
	enum value {
		NOTHING,
		ONE_PAIR,
		TWO_PAIR,
		THREE_OF_A_KIND,
		STRAIGHT,
		FLUSH,
		FULL_HOUSE,
		FOUR_OF_A_KIND,
		STRAIGHT_FLUSH,
		FIVE_OF_A_KIND
	};

public:
	Hand();
	virtual ~Hand();
	void addCard(Card* card);
	std::vector<Card*> getCards() {
		return cards;
	}
	const char* EnumStrings[10] = { "nothing", "one pair", "two pair",
			"three of a kind", "straight", "flush", "full house",
			"four of a kind", "straight flush", "five of a kind" };
	const char * getTextForEnum(int enumVal);
	const char* evaluate();
private:
	std::vector<Card*> cards;
	const int countCardsWithSameValue();
	const bool cardsFormFullHouse();
	const bool cardsFormFlush();
	const bool cardsFormStraightFlush();
	const bool cardsFormStraight();
	const bool cardsFormTwoPair();
	void sortHand();
};

#endif /* SRC_HAND_H_ */
