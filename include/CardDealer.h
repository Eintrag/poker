#ifndef CARDDEALER_H
#define CARDDEALER_H
#include "Card.h"
#include <vector>

class CardDealer {
public:
	CardDealer();
	virtual ~CardDealer();
	std::vector<Card*> dealCards(unsigned short numberOfCards);
protected:

private:
	Card* pickRandomCard();
};

#endif // CARDDEALER_H
